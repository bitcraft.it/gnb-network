"use strict";

var validator = require("validator");

class State {
    /**
     * Build a state instance
     * @param name {string} the state name
     * @param trigger {string} the state trigger
     */
    constructor(name, trigger) {
        this.setName(name);
        this.setTrigger(trigger);
    }

    /**
     * Set the name of the state
     * @param name {string} the state name
     * @throws {string} error message
     */
    setName(name) {
        if (validator.isEmpty(name, { ignore_whitespace: true })) throw "The state's name cannot be empty.";
        if (!validator.isLength(name, { min: 1, max: 15 })) throw "The state's name is too long.";

        this.name = name;
    }

    /**
     * Get the minimum number of the trigger
     * if it exists
     * @returns {number | null}
     */
    getMinTrigger() {
        if (this.trigger.includes("<=")) {
            return parseFloat(this.trigger.split("%v")[0].replace("<=%v", ""));
        }

        return null;
    }

    /**
     * Get the maximum value of the trigger
     * if it exists
     * @returns {number | null}
     */
    getMaxTrigger() {
        let split = this.trigger.split("%v");

        if (split.length === 2) {
            return parseFloat(split[1].replace("<", ""));
        } else if (!split[0].includes("<=")) {
            return parseFloat(split[0].replace("<", ""));
        }

        return null;
    }

    /**
     *
     * Set the trigger expression which fires the state
     * @param trigger {string} the state trigger
     * @throws {string} error message
     */
    setTrigger(trigger) {
        const regexp = /(([+-]?\d+(.\d+)?)(<=)(%[v])((<)[+-]?\d+(.\d+)?)?)|((%[v])(<)([+-]?(\d+(.\d+)?)))/;
        if (!regexp.test(trigger)) throw "The state's trigger is not valid";

        this.trigger = trigger;

        let min = this.getMinTrigger();
        let max = this.getMaxTrigger();

        if (min && max && min >= max) throw "The state's trigger is not valid.";
    }

    /**
     * Check whether the trigger has been fired or not
     * @param currentValue {number} the current node value to check
     * @return {boolean} true, if the trigger is fired, false otherwise
     */
    isTriggered(currentValue) {
        let split = this.trigger.split("%v").filter(val => val !== "");

        if (split.length === 1) {
            return eval(this.trigger.replace("%v", currentValue));
        } else {
            return eval(split[0] + currentValue) && eval(currentValue + split[1]);
        }
    }

    /**
     * Get a state instance from a JSON containing
     * its definition
     * @param json {JSON} the json definition
     * @return {State} the state instance
     * @throws {string} error message
     */
    static fromJSON(json) {
        if (!json.name) throw "The state's name cannot be empty.";
        if (!json.trigger) throw "The state's trigger cannot be empty.";

        return new State(json.name, json.trigger);
    }
}
exports.State = State;