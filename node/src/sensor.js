"use strict";

var validator = require("validator");

class Sensor {
    /**
     * Build a sensor instance
     * @param json {JSON} the json definition of the sensor
     * @throws {string} error message
     */
    constructor(json) {
        let paramLength = Object.keys(json).length;

        if (paramLength > 0) {
            if (!json.databaseSensorType) throw "The sensor's database type cannot be empty.";
            if (!json.databaseSensorName) throw "The sensor's database name cannot be empty.";
            if (!json.databaseSensorUrl) throw "The sensor's database url cannot be empty.";
            if (!json.databaseSensorUser) throw "The sensor's database user cannot be empty.";
            if (!json.databaseSensorPassword) throw "The sensor's database password cannot be empty.";
            if (!json.databaseSensorTable) throw "The sensor's database table cannot be empty.";
            if (!json.databaseSensorColumn) throw "The sensor's database column cannot be empty.";

            this.setDatabaseFromData(json.databaseSensorType, json.databaseSensorName, json.databaseSensorUrl, json.databaseSensorUser, json.databaseSensorPassword, json.databaseSensorTable, json.databaseSensorColumn);
        }
    }

    /**
     * Set the database credentials
     * @param databaseSensorType {string} the database type where the sensor is located
     * @param databaseSensorName {string} the database name (as it is called on Grafana's database list) where the nodes' probabilities will be registered
     * @param databaseSensorUrl {string} url of the database where the sensor is located
     * @param databaseSensorUser {string} user of the database where the sensor is located
     * @param databaseSensorPassword {string} password of the database where the sensor is located
     * @param databaseSensorTable {string} table of the database where the sensor is located
     * @param databaseSensorColumn {string} column of the database where the sensor is located
     * @throws {string} error message
     */
    setDatabaseFromData(databaseSensorType, databaseSensorName, databaseSensorUrl, databaseSensorUser, databaseSensorPassword, databaseSensorTable, databaseSensorColumn) {
        if (!validator.isURL(databaseSensorUrl)) throw "The sensor's database URL is not valid.";

        this.databaseSensorType = databaseSensorType;
        this.databaseSensorName = databaseSensorName;
        this.databaseSensorUrl = databaseSensorUrl;
        this.databaseSensorUser = databaseSensorUser;
        this.databaseSensorPassword = databaseSensorPassword;
        this.databaseSensorTable = databaseSensorTable;
        this.databaseSensorColumn = databaseSensorColumn;
    }

    /**
     * Get whether the sensor is set or not
     * @returns {boolean} true, if the sensor is set, false otherwise
     */
    isSet() {
        return Object.keys(this).length !== 0;
    }

    /**
     * Get a sensor instance from a JSON containing
     * its definition
     * @param json {JSON} the json definition
     * @return {Sensor} the sensor instance
     */
    static fromJSON(json) {
        return new Sensor(json);
    }
}
exports.Sensor = Sensor;