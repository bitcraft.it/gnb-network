"use strict";

var _state = require("../state");

var State = _state.State;
class StateBuilder {
    /**
     * Build a state instance step by step
     * using a builder pattern
     */
    constructor() {
        this.name = null;
        this.trigger = null;
    }

    /**
     * Set the name of the state
     * @param name {string} name of the state
     * @returns {StateBuilder} the builder instance
     */
    withName(name) {
        this.name = name;
        return this;
    }

    /**
     * Set the min trigger of the state
     * @param minTrigger {number} min trigger of the state
     * @returns {StateBuilder} the builder instance
     */
    withMinTrigger(minTrigger) {
        this.minTrigger = minTrigger;
        return this;
    }

    /**
     * Set the max trigger of the state
     * @param maxTrigger {number} max trigger of the state
     * @returns {StateBuilder} the builder instance
     */
    withMaxTrigger(maxTrigger) {
        this.maxTrigger = maxTrigger;
        return this;
    }

    /**
     * Build a state instance
     * @returns {State} the state instance
     * @throws {string} error message
     */
    build() {
        if (!this.minTrigger && !this.maxTrigger) throw "The state's trigger cannot be empty.";

        this.trigger = this.minTrigger ? this.minTrigger + "<=%v" : "%v";
        if (this.maxTrigger) this.trigger += "<" + this.maxTrigger;

        return State.fromJSON(JSON.parse(JSON.stringify(this)));
    }
}
exports.StateBuilder = StateBuilder;