/**
 * Generate a random 5 character UID
 * @returns {string} the random UID
 */
export function generateUID() {
    return Date.now().toString(36);
}