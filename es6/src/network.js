import { Node } from "./node";
import * as jsbayes from 'jsbayes';
import validator from 'validator';

export class Network {
    /**
     * Build a network instance
     * @param id {string} the network id
     * @param name {string} the network name
     * @param refreshTime {number} the interval time (in milliseconds) between the node updates
     * @param databaseWriteType {string} the database type where the nodes' probabilities will be registered
     * @param databaseWriteName {string} the database name (as it is called on Grafana's database list) where the nodes' probabilities will be registered
     * @param databaseWriteUrl {string} url of the database where the nodes' probabilities will be registered
     * @param databaseWriteUser {string} user of the database where the nodes' probabilities will be registered
     * @param databaseWritePassword {string} password of the database where the nodes' probabilities will be registered
     * @param nodes {JSON[]} the list of nodes
     */
    constructor(id, name, refreshTime, databaseWriteType, databaseWriteName, databaseWriteUrl, databaseWriteUser, databaseWritePassword, nodes) {
        this.setId(id);
        this.setName(name);
        this.setDatabaseFromData(databaseWriteType, databaseWriteName, databaseWriteUrl, databaseWriteUser, databaseWritePassword);
        this.setRefreshTime(refreshTime);
        this.setNodeList(nodes);

        if(this.hasCycle()) throw "The network contains a cycle.";

        this.makeGraph();
    }

    /**
     * Add the list of states to a graph node
     * @param graph {JGraph} the graph to edit
     */
    addStatesToGraphNodes(graph){
        this.nodes.forEach(node => {
            let states = node.stateList.map(state => state.name);
            graph.addNode(node.id, states);
        });
    }

    /**
     * Set the cpt list of a graph node
     * @param graph {JGraph} the graph to edit
     * @param sourceNode {Node} the network's node
     */
    static setGraphNodeCPTFromNode(graph, sourceNode){
        let graphNode = graph.node(sourceNode.id);

        if(sourceNode.cptList.length === 1) {
            graphNode.setCpt(sourceNode.cptList[0]);
        } else {
            graphNode.setCpt(sourceNode.cptList);
        }
    }

    /**
     * Set the parent list of a graph node
     * @param graph {JGraph} the graph to edit
     * @param sourceNode {Node} the network's node
     */
    static setGraphNodeParentsFromCorrespondingNode(graph, sourceNode){
        let graphNode = graph.node(sourceNode.id);
        sourceNode.parentList.forEach(parent => graphNode.addParent(graph.node(parent)));
    }

    /**
     * Create a new graph object
     * @returns {JGraph} the graph object
     */
    makeGraph() {
        let graph = jsbayes.newGraph();

        this.addStatesToGraphNodes(graph);
        this.nodes.forEach(node => {
            Network.setGraphNodeParentsFromCorrespondingNode(graph, node);
            Network.setGraphNodeCPTFromNode(graph, node);
        });

        this.graph = graph;
    }

    /**
     * Set the id of the network
     * @param id {string} the network id
     * @throws {string} error message
     */
    setId(id) {
        if(!validator.isAlphanumeric(id)) throw "The network's ID must be alpha-numerical.";

        this.id = id;
    }

    /**
     * Set the name of the network
     * @param name {string} the network name
     * @throws {string} error message
     */
    setName(name) {
        if(validator.isEmpty(name, {ignore_whitespace: true})) throw "The network's name cannot be empty.";
        if(!validator.isLength(name, {min: 1, max: 30})) throw "The network's name is too long.";

        this.name = name;
    }

    /**
     * Set the refresh time of the network
     * @param refreshTime {number} the refresh time
     * @throws {string} error message
     */
    setRefreshTime(refreshTime) {
        if(!validator.isInt(refreshTime)) throw "The network's refresh time must be a number.";
        if(!validator.isInt(refreshTime, {min: 1000})) throw "The network's refresh time must be at least of 1000ms.";

        this.refreshTime = refreshTime;
    }

    /**
     * Set the database credentials
     * @param databaseWriteType {string} the database type where the nodes' probabilities will be registered
     * @param databaseWriteName {string} the database name (as it is called on Grafana's database list) where the nodes' probabilities will be registered
     * @param databaseWriteUrl {string} url of the database where the nodes' probabilities will be registered
     * @param databaseWriteUser {string} user of the database where the nodes' probabilities will be registered
     * @param databaseWritePassword {string} password of the database where the nodes' probabilities will be registered
     * @throws {string} error message
     */
    setDatabaseFromData(databaseWriteType, databaseWriteName, databaseWriteUrl, databaseWriteUser, databaseWritePassword) {
        if(!validator.isURL(databaseWriteUrl)) throw "The network's database URL is not valid.";

        this.databaseWriteType = databaseWriteType;
        this.databaseWriteName = databaseWriteName;
        this.databaseWriteUrl = databaseWriteUrl;
        this.databaseWriteUser = databaseWriteUser;
        this.databaseWritePassword = databaseWritePassword;
    }

    /**
     * Set the node list of the network
     * @param nodeList {JSON[]} the node list
     * @throws {string} error message
     */
    setNodeList(nodeList) {
        if(!Array.isArray(nodeList)) throw "The network's node list is not valid.";

        this.nodes = [];
        nodeList.forEach(node => this.addNode(Node.fromJSON(node)));
    }

    /**
     * Add a node to the network
     * @param nodeObject {Node} the node to add
     * @throws {string} error message
     */
    addNode(nodeObject) {
        this.nodes.forEach(node => {
            if(node.id === nodeObject.id) throw "The node's ID already exists.";
            if(node.name === nodeObject.name) throw "The node's name already exists.";
        });

        this.nodes.push(nodeObject);
    }

    /**
     * Get a node with the given ID
     * @param nodeID {string} the node ID
     * @returns {Node} the node
     */
    getNode(nodeID) {
        return this.nodes.find(node => node.id === nodeID);
    }

    /**
     * Get the amount of nodes in the network
     * @returns {number} amount of nodes
     */
    getNodeLength() {
        return this.nodes.length;
    }

    /**
     * Remove a node from the network
     * @param nodeObject {Node} the node to remove
     */
    removeNodeFromObject(nodeObject) {
        this.removeNodeFromId(nodeObject.id);
    }

    /**
     * Remove a node from the network
     * @param nodeID {string} the ID of the node to remove
     * @throws {string} error message
     */
    removeNodeFromId(nodeID) {
        if(!this.nodes.find(node => node.id === nodeID)) throw "There is not a node with this ID.";

        this.nodes = this.nodes.filter(node => node.id !== nodeID);
    }

    /**
     * Get whether the network contains an isolated node or not
     * @returns {boolean} true, if the network has an isolated node | false otherwise
     */
    hasNodeIsolated() {
        let isolated = false;

        for(let i=0; i<this.getNodeLength() && !isolated; i++) {
            if(!this.nodes[i].hasParents()) {
                let successors = false;

                this.nodes.filter(nodeFilter => nodeFilter !== this.nodes[i]).forEach(nodeCheck => {
                    if(nodeCheck.hasParent(this.nodes[i].id)) successors = true;
                });

                if(!successors) isolated = true;
            }
        }

        return isolated;
    }

    /**
     * Get whether the network contains a cycle with its nodes
     * @returns {boolean} true, if the network has a cycle | false otherwise
     */
    hasCycle() {
        for(let i=0; i < this.nodes.length; i ++){
            if(this.isThereCycle(this.nodes[i], this.nodes[i].parentList, this.nodes))
                return true;
        }
        return false;
    }

    /**
     * Get whether a parent node contains its own child node as a parent
     * This function is auxiliary for the hasCycle method
     * @param nodeToCheck {Node} the node to check
     * @param parentList {string[]}
     * @param nodeList {Node[]}
     * @returns {boolean} true, if the parent node has a child as parent | false otherwise
     */
    isThereCycle(nodeToCheck, parentList, nodeList){
        if(parentList.length === 0) return false;

        for(let i=0; i < parentList.length; i++){
            if(parentList[i] === nodeToCheck.id) return true;

            let node = nodeList.find(nodeToFind => nodeToFind.id === parentList[i]);

            if(this.isThereCycle(nodeToCheck, node.parentList, nodeList)) return true;
        }
        return false;
    }

    /**
     * Get a JSON definition of the network instance
     * @returns {JSON} the JSON definition
     */
    getJSON() {
        return JSON.parse(
            JSON.stringify(this, (key, value) => {
                return key === 'graph' ? undefined : value;
            })
        );
    }

    /**
     * Get a network instance from a JSON containing
     * its definition
     * @param json {JSON} the json file
     * @returns {Network} the network instance
     * @throws {string} error message
     */
    static fromJSON(json) {
        if(!json.id) throw "The network's ID cannot be empty.";
        if(!json.name) throw "The network's name cannot be empty.";
        if(!json.refreshTime) throw "The network's refresh time cannot be empty.";
        if(!json.databaseWriteType) throw "The network's database type cannot be empty.";
        if(!json.databaseWriteName) throw "The network's database name cannot be empty.";
        if(!json.databaseWriteUrl) throw "The network's database url cannot be empty.";
        if(!json.databaseWriteUser) throw "The network's database user cannot be empty.";
        if(!json.databaseWritePassword) throw "The network's database password cannot be empty.";
        if(!json.nodes) throw "The network's node list cannot be empty.";

        return new Network(
            json.id, json.name, json.refreshTime,
            json.databaseWriteType, json.databaseWriteName, json.databaseWriteUrl, json.databaseWriteUser, json.databaseWritePassword,
            json.nodes
        );
    }
}
