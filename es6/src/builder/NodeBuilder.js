import {Node} from "../node";
import {Sensor} from "../sensor";

export class NodeBuilder {
    /**
     * Build a node instance step by step
     * using a builder pattern
     */
    constructor() {
        this.id = null;
        this.name = null;
        this.parentList = null;
        this.stateList = null;
        this.cptList = null;
        this.sensor = {};
    }

    /**
     * Set the id of the node
     * @param id {string} id of the node
     * @returns {NodeBuilder} the builder instance
     */
    withId(id) {
        this.id = id;
        return this;
    }

    /**
     * Set the name of the node
     * @param name {string} name of the node
     * @returns {NodeBuilder} the builder instance
     */
    withName(name) {
        this.name = name;
        return this;
    }

    /**
     * Set the parent list of the node
     * @param parentList {string[]} parents of the node
     * @returns {NodeBuilder} the builder instance
     */
    withParents(parentList) {
        this.parentList = parentList;
        return this;
    }

    /**
     * Set the state list of the node
     * @param stateList {State[]} states of the node
     * @returns {NodeBuilder} the builder instance
     */
    withStates(stateList) {
        this.stateList = stateList;
        return this;
    }

    /**
     * Set the cpt list of the node
     * @param cptList {array} cpt of the node
     * @returns {NodeBuilder} the builder instance
     */
    withCpt(cptList) {
        this.cptList = cptList;
        return this;
    }

    /**
     * Set the sensor of the node
     * @param sensor {Sensor} sensor of the node
     * @returns {NodeBuilder} the builder instance
     */
    withSensor(sensor) {
        this.sensor = sensor;
        return this;
    }

    /**
     * Build the node instance
     * @returns {Node} the node instance
     */
    build() {
        return Node.fromJSON(JSON.parse(JSON.stringify(this)));
    }
}