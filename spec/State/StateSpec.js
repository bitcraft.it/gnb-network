const {State} = require('../../node/src/state');
const {jsonString} = require('../helpers/exampleJSON');

let jsonObj = JSON.parse(jsonString);

describe('State', () => {
    let state0;
    let state1;
    let state2;

    beforeAll(() => {
        state0 = State.fromJSON(jsonObj.nodes[0].states[0]);
        state1 = State.fromJSON(jsonObj.nodes[0].states[1]);
        state2 = State.fromJSON(jsonObj.nodes[0].states[2]);
    });

    describe('when it is in state 0', () => {

        describe('when the value is < 90', () => {
            it('should not be triggered', () => {
                expect(state0.isTriggered(70)).toEqual(false);
            });
        });

        describe('when the value is >= 90', () => {
            it('should be triggered', () => {
                expect(state0.isTriggered(90)).toEqual(true);
            });
        });
    });

    describe('when it is in state 1', () => {

        describe('when the value is <= 40 and >= 90', () => {
            it('should not be triggered', () => {
                expect(state1.isTriggered(40)).toEqual(false);
                expect(state1.isTriggered(90)).toEqual(false);
            });
        });

        describe('when the value is > 40 and < 90', () => {
            it('should be triggered', () => {
                expect(state1.isTriggered(50)).toEqual(true);
            });
        });
    });

    describe('when it is in state 2', () => {

        describe('when the value is > 40', () => {
            it('should not be triggered', () => {
                expect(state2.isTriggered(50)).toEqual(false);
            });
        });

        describe('when the value is <= 40', () => {
            it('should be triggered', () => {
                expect(state2.isTriggered(40)).toEqual(true);
            });
        });
    });
});