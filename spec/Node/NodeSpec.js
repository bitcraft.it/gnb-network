const {Node} = require('../../node/src/node');
const {jsonString} = require('../helpers/exampleJSON');

let jsonObj = JSON.parse(jsonString);

describe('Node', () => {
    let node0;
    let node2;

    beforeAll(() => {
        node0 = Node.fromJSON(jsonObj.nodes[0]);
        node2 = Node.fromJSON(jsonObj.nodes[2]);
    });

    describe('when it has not a sensor', () => {
        it('should indicate that the node has no sensor', () => {
            expect(node2.hasSensor()).toEqual(false);
        });
    });

    describe('when it has a sensor', () => {
        it('should indicate that the node has a sensor', () => {
            expect(node0.hasSensor()).toEqual(true);
        });

        it('should have right state\'s names', () => {
            expect(node0.getState(90)).toEqual('State 0');
            expect(node0.getState(60)).toEqual('State 1');
            expect(node0.getState(40)).toEqual('State 2');
        });
    });
});